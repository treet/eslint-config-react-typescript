module.exports = {
  parser: '@typescript-eslint/parser',
  extends: [require.resolve('eslint-config-react-app'), require.resolve('@treet/eslint-config-typescript')],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'react/prop-types': 'off',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
};
