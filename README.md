# @treet/eslint-config-react-typescript

[![pipeline status][pipeline-image]][pipeline-url]

Shared [ESLint] configuration used by Treet in React projects.

[pipeline-url]: https://gitlab.com/treet/eslint-config-react-typescript/commits/master
[pipeline-image]: https://gitlab.com/treet/eslint-config-react-typescript/badges/master/pipeline.svg
[ESLint]: https://eslint.org
